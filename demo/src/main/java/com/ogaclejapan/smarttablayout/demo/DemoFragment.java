package com.ogaclejapan.smarttablayout.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;

public class DemoFragment extends Fragment {
  WebView mWebView;

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_demo, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    int position = FragmentPagerItem.getPosition(getArguments());

    mWebView = (WebView) view.findViewById(R.id.webview);
    WebSettings webSettings = mWebView.getSettings();//javascript有効
    webSettings.setJavaScriptEnabled(true);//javascript有効
    //TextView title = (TextView) view.findViewById(R.id.item_title);
    //title.setText(String.valueOf(position));

    //webview内で遷移させる
    mWebView.setWebViewClient(new WebViewClient(){
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
      }
    });

    if (position == 0) {
      mWebView.loadUrl("https://manga.line.me/");
    } else if (position == 1) {
      mWebView.loadUrl("http://www.yahoo.co.jp/");
    }
  }

  //参考：http://stackoverflow.com/questions/19256137/android-webview-cangoback-error-with-actionbarsherlock-fragment?lq=1
  public boolean canGoBack() {
    return  ( mWebView != null ) && mWebView.canGoBack();
  }

  public boolean GoBack(){
    mWebView.goBack();
    return true;
  }

}
